package com.ABC.ShippingCarSystem.Controller;

import com.ABC.ShippingCarSystem.Model.Solicitud;
import com.ABC.ShippingCarSystem.Service.SolicitudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/solicitud")
public class SolicitudController {

    @Autowired
    private SolicitudService solicitudService;

    @GetMapping("/all")
    public List<Solicitud> getLoteList(){
        return solicitudService.getSolicitudList();
    }

    @PostMapping("/add")
    public List<Solicitud> addLote(@RequestBody final Solicitud solicitud){
        solicitudService.addSolicitud(solicitud);
        return solicitudService.getSolicitudList();
    }

    @DeleteMapping("/delete")
    public List<Solicitud> deleteLote(@RequestBody final Solicitud solicitud){
        return solicitudService.getSolicitudList();
    }
}
