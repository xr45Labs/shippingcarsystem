package com.ABC.ShippingCarSystem.Controller;

import com.ABC.ShippingCarSystem.Model.Auto;
import com.ABC.ShippingCarSystem.Service.AutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/auto")
public class AutoController {

    @Autowired
    private AutoService autoService;

    @GetMapping("/all")
    public List<Auto> getAutoList(){
        return autoService.getAutoList();
    }

    @PostMapping("/add")
    public List<Auto> addAuto(@RequestBody final Auto auto){
        autoService.addAuto(auto);
        return autoService.getAutoList();
    }

    @DeleteMapping("/delete")
    public List<Auto> deleteAuto(@RequestBody final Auto auto){
        return autoService.getAutoList();
    }
}
