package com.ABC.ShippingCarSystem.Controller;

import com.ABC.ShippingCarSystem.Model.Transporte;
import com.ABC.ShippingCarSystem.Service.TransporteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/transporte")
public class TransporteController {
    @Autowired
    private TransporteService transporteService;

    @GetMapping("/all")
    public List<Transporte> getTransporteList(){
        return transporteService.getTransporteList();
    }

    @PostMapping("/add")
    public List<Transporte> addTransporte(@RequestBody final Transporte transporte){
        transporteService.addTransporte(transporte);
        return transporteService.getTransporteList();
    }

    @DeleteMapping("/delete")
    public List<Transporte> deleteTransporte(@RequestBody final Transporte transporte){
        return transporteService.getTransporteList();
    }
}
