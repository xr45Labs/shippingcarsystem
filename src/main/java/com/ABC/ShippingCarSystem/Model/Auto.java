package com.ABC.ShippingCarSystem.Model;

import javax.persistence.*;

@Entity
public class Auto{
    Integer autoId;
    String marca;
    String modelo;
    String transmision;
    String descripcionEstetica;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    public Integer getAutoId() {
        return autoId;
    }

    public void setAutoId(Integer autoId) {
        this.autoId = autoId;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTransmision() {
        return transmision;
    }

    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }

    public String getDescripcionEstetica() {
        return descripcionEstetica;
    }

    public void setDescripcionEstetica(String descripcionEstetica) {
        this.descripcionEstetica = descripcionEstetica;
    }
}
