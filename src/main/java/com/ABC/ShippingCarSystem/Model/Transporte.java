package com.ABC.ShippingCarSystem.Model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Transporte{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transporteId")
    Integer transporteId;
    String empresa;
    String chofer;
    String placas;


    public Integer getTransporteId() {
        return transporteId;
    }

    public void setTransporteId(Integer transporteId) {
        this.transporteId = transporteId;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getChofer() {
        return chofer;
    }

    public void setChofer(String chofer) {
        this.chofer = chofer;
    }

    public String getPlacas() {
        return placas;
    }

    public void setPlacas(String placas) {
        this.placas = placas;
    }

}
