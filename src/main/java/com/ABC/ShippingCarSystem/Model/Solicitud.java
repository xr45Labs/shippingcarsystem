package com.ABC.ShippingCarSystem.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Solicitud{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "solicitudId")
    private Integer solicitudId;
    private Date fecha;
    private String status;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "solicitudId", referencedColumnName="solicitudId")
    private List<Transporte> transporteList;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "solicitudId", referencedColumnName = "solicitudId")
    private List<Auto> autoList;

    public Integer getSolicitudId() {
        return solicitudId;
    }

    public void setSolicitudId(Integer solicitudId) {
        this.solicitudId = solicitudId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Transporte> getTransporteList() {
        return transporteList;
    }

    public void setTransporteList(List<Transporte> transporteList) {
        this.transporteList = transporteList;
    }

    public List<Auto> getAutoList() {
        return autoList;
    }

    public void setAutoList(List<Auto> autoList) {
        this.autoList = autoList;
    }
}
