package com.ABC.ShippingCarSystem.Repository;

import com.ABC.ShippingCarSystem.Model.Transporte;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransporteRepository extends JpaRepository<Transporte,Integer> {
}
