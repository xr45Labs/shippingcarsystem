package com.ABC.ShippingCarSystem.Repository;

import com.ABC.ShippingCarSystem.Model.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SolicitudRepository extends JpaRepository<Solicitud,Integer> {
}
