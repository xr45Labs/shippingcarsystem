package com.ABC.ShippingCarSystem.Repository;

import com.ABC.ShippingCarSystem.Model.Auto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutoRepository extends JpaRepository<Auto,Integer> {
}
