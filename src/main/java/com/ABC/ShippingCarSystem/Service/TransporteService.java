package com.ABC.ShippingCarSystem.Service;

import com.ABC.ShippingCarSystem.Model.Transporte;
import com.ABC.ShippingCarSystem.Repository.TransporteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransporteService {
    @Autowired
    private TransporteRepository transporteRepository;

    public List<Transporte> getTransporteList(){
        return transporteRepository.findAll();
    }

    public List<Transporte> addTransporte(Transporte transporte){
        transporteRepository.save(transporte);
        return getTransporteList();
    }

    public List<Transporte> deleteTransporte(Transporte transporte){
        transporteRepository.delete(transporte);
        return getTransporteList();
    }
}
