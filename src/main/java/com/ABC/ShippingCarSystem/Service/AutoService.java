package com.ABC.ShippingCarSystem.Service;

import com.ABC.ShippingCarSystem.Model.Auto;
import com.ABC.ShippingCarSystem.Repository.AutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AutoService {
    @Autowired
    private AutoRepository autoRepository;

    public List<Auto> getAutoList(){
        return autoRepository.findAll();
    }

    public List<Auto> addAuto(Auto auto){
        autoRepository.save(auto);
        return getAutoList();
    }

    public List<Auto> deleteAuto(Auto auto){
        autoRepository.delete(auto);
        return getAutoList();
    }


}
