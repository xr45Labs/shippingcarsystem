package com.ABC.ShippingCarSystem.Service;

import com.ABC.ShippingCarSystem.Model.Solicitud;
import com.ABC.ShippingCarSystem.Repository.SolicitudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SolicitudService {
    @Autowired
    private SolicitudRepository solicitudRepository;

    public List<Solicitud> getSolicitudList(){
        return solicitudRepository.findAll();
    }

    public List<Solicitud> addSolicitud(Solicitud solicitud){
        solicitudRepository.save(solicitud);
        return getSolicitudList();
    }

    public List<Solicitud> delete(Solicitud solicitud){
        solicitudRepository.delete(solicitud);
        return getSolicitudList();
    }
}
